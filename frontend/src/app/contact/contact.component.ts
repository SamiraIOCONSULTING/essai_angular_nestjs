import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Contact } from '../contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  displayedColumns  :  string[] = ['id', 'firstname', 'lastname', 'email', 'phone', 'address', 'actions'];
  dataSource  = [] as any;
  contact:Contact=new Contact();

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.readContacts().subscribe((result)=>{   
      this.dataSource = result;
    })
  }

  selectContact(contact: Contact){
    this.contact = contact;
  }

  newContact(){
    this.contact = new Contact();
  }

  createContact(){
    this.apiService.createContact(this.contact).subscribe((result)=>{
      console.log(result);
      this.dataSource.push(result);
      this.ngOnInit()
    });
    
  }

  deleteContact(id: number){
    this.apiService.deleteContact(id).subscribe((result)=>{
      console.log(result);
      this.ngOnInit()
    });
  }

  updateContact(){
      this.apiService.updateContact(this.contact).subscribe((result)=>{
      console.log(result);
    });
  }

}
